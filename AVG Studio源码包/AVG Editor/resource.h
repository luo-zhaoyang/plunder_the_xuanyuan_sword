//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by Editor.rc
//
#define IDD_ABOUTBOX                    100
#define IDP_OLE_INIT_FAILED             100
#define IDR_MAINFRAME                   128
#define IDR_EDITORTYPE                  129
#define IDB_TREADMARD                   130
#define IDD_ACTLIST                     131
#define IDD_CLULIST                     132
#define IDD_ACTDLG                      134
#define IDD_SET                         135
#define IDD_CLUDLG                      136
#define IDR_POPUP                       137
#define IDB_ILLUSTRATE                  138
#define IDC_EDIT                        1001
#define IDC_RADIO1                      1002
#define IDC_RADIO2                      1003
#define IDC_EDIT1                       1007
#define IDC_EDIT3                       1008
#define IDC_EDIT4                       1009
#define IDC_EDIT5                       1010
#define IDC_EDIT6                       1012
#define IDC_COMBO                       1013
#define IDC_TEXT1                       1014
#define IDC_TEXT2                       1015
#define IDC_TEXT3                       1016
#define IDC_TEXT4                       1017
#define IDC_TEXT5                       1018
#define IDC_TEXT6                       1019
#define IDC_EDIT2                       1020
#define IDC_LIST                        1021
#define IDC_BMP                         1026
#define IDC_BMPTEXT                     1027
#define ID_EDITONE                      32772
#define ID_INSERT                       32773
#define ID_DELONE                       32774
#define ID_ADDONE                       32775

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_3D_CONTROLS                     1
#define _APS_NEXT_RESOURCE_VALUE        139
#define _APS_NEXT_COMMAND_VALUE         32776
#define _APS_NEXT_CONTROL_VALUE         1028
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
