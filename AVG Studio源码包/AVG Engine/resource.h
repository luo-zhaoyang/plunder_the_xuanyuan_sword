//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by AVG.rc
//
#define IDC_MYICON                      2
#define IDEXIT                          2
#define IDOPEN                          3
#define IDSAVE                          4
#define IDD_AVG_DIALOG                  102
#define IDD_ABOUTBOX                    103
#define IDS_APP_TITLE                   103
#define IDM_ABOUT                       104
#define IDM_EXIT                        105
#define IDS_HELLO                       106
#define IDI_AVG                         107
#define IDI_SMALL                       108
#define IDC_AVG                         109
#define IDR_MAINFRAME                   128
#define IDB_BITMAP1                     130
#define IDC_STATIC                      -1

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        131
#define _APS_NEXT_COMMAND_VALUE         32773
#define _APS_NEXT_CONTROL_VALUE         1000
#define _APS_NEXT_SYMED_VALUE           110
#endif
#endif
