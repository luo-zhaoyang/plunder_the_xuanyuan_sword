// Bmp.h: interface for the CBmp class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(__BMP_H__)
#define __BMP_H__

class CBmp
{
private:
// 存盘时需要纪录的数据
	// 图片的显示位置
	int m_x;
	int m_y;
	// 显示的宽度
	int m_w;
	// 显示的高度
	int m_h;
	// 在源位图上的位置
	int m_sx;
	int m_sy;
	// 是否显示
	BOOL m_bShow;
	// 当前图片的文件名
	char m_psFileName[256];
////
	// 用于存放位图的设备
	CDC m_MemDC;

public:
	CBmp();
	virtual ~CBmp();

public:
	// 初始化，打开资源文件，构造必要的DC
	BOOL Init( CDC* pDC );

	// 打开文件，设置显示位置
	BOOL Open( char* psFileName, int x, int y, int sx=0, int sy=0, int sw=0, int sh=0 );

	// 绘制
	void Draw( CDC* pDC, int x, int y );

	// 设置是否显示
	BOOL Show( BOOL bShow );

	// 将本类保存至字符串
	BOOL SaveToString( CString& strFile );

	// 从字符串还原本类
	BOOL LoadFromString( CString& strFile );
};

#endif // !defined(__BMP_H__)
