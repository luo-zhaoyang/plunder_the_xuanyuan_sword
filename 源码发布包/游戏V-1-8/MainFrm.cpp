// MainFrm.cpp : implementation of the CMainFrame class
//

#include "stdafx.h"
#include "game.h"
#include "MIDI.H"
#include "MainFrm.h"

#include <atlimage.h>//生成png文件用
#include <imm.h>//屏蔽中文输入法用
#pragma comment (lib, "imm32.lib")

HIMC g_hImc;
void DisableIME(HWND hWnd)
{
    g_hImc = ImmGetContext(hWnd);
    if (g_hImc) {
        ImmAssociateContext(hWnd, NULL);
	}
    ImmReleaseContext(hWnd, g_hImc);
    ::SetFocus(hWnd);
}


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#include "Enemy.h"

/////////////////////////////////////////////////////////////////////////////
// CMainFrame

IMPLEMENT_DYNCREATE(CMainFrame, CFrameWnd)

BEGIN_MESSAGE_MAP(CMainFrame, CFrameWnd)
	ON_WM_CREATE()
	ON_WM_MOUSEMOVE()
	ON_WM_RBUTTONDOWN()
	ON_WM_RBUTTONUP()
	ON_WM_LBUTTONUP()
	ON_WM_LBUTTONDOWN()
	//ON_WM_TIMER()
	ON_WM_PAINT()
	ON_COMMAND(ID_KILL, OnKill)
	ON_MESSAGE(ID_ACCIDENT, OnAccident)
	ON_COMMAND(ON_EXIT, OnExit)
	ON_COMMAND(ON_MAP, OnMap)
	ON_COMMAND(ON_SNAPSHOT, OnSnapshot)
	ON_WM_LBUTTONDBLCLK()
    ON_WM_SETCURSOR()
    ON_WM_NCMOUSEMOVE()
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CMainFrame construction/destruction

CMainFrame::CMainFrame()
{
	m_pMidi = NULL;
}

CMainFrame::~CMainFrame()
{
	if (m_pMidi) {
		delete m_pMidi;
		m_pMidi = NULL;
	}

	int i;
	for (i=0 ; i<50 ; i++){
		delete m_enemies[i];
	}
}

BOOL CMainFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	if( !CFrameWnd::PreCreateWindow(cs) )
		return FALSE;
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs | FWS_ADDTOTITLE

	cs.style = WS_OVERLAPPED | WS_CAPTION | WS_SYSMENU;
	//控制窗口生成的位置和尺寸（在800*600分辨率下正好不留上、左、右边框）
	cs.hMenu=0;//菜单栏高度为0
	cs.cx=812+(GetSystemMetrics(SM_CXSIZEFRAME)*2);
	cs.cy=583+GetSystemMetrics(SM_CYCAPTION)+GetSystemMetrics(SM_CYSIZEFRAME);
	cs.x=((GetSystemMetrics(SM_CXSCREEN))/2 - cs.cx/2);//初始位置居中显示
	cs.y=((GetSystemMetrics(SM_CYSCREEN))/2 - cs.cy/2);
	//屏幕的用户区为800*567像素

	return TRUE;
}

/////////////////////////////////////////////////////////////////////////////
// CMainFrame diagnostics

#ifdef _DEBUG
void CMainFrame::AssertValid() const
{
	CFrameWnd::AssertValid();
}

void CMainFrame::Dump(CDumpContext& dc) const
{
	CFrameWnd::Dump(dc);
}

#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CMainFrame message handlers

int CMainFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CFrameWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	// 重要 ！！！下列代码禁用中文输入法
	// 如果不禁用输入法，快捷键ASDF默认输入中文
	DisableIME (GetSafeHwnd());

	this->SetWindowText("劫掠轩辕剑（复刻版）");

	m_pMidi = new CMidi;
	m_pMidi->Init(m_hWnd);

	//初始化敌人组
	int i;
	for (i=0 ; i<50 ; i++){
		m_enemies[i] = new CEnemy;
	}

	//+█/////////////////////////////////////////////////////////////////////////////
	blood=0;
	level=0;
	score=0;
	bullet=0;
	super_bullet=FALSE;
	st=0;
	timedelay=0;
	messagedelay=0;
	bmp_start.LoadBitmap(IDB_BMP_START);//调入设计局声明
	bmp_Info.LoadBitmap(IDB_BMP_INFO);//调入信息条图片
	bmp_title.LoadBitmap(IDB_BMP_TITLE);//调入版权声明
	bmp_end.LoadBitmap(IDB_BMP_END);//调入结束声明
	bmp_Bomb.LoadBitmap(IDB_BMP_BOMB);//调入爆炸动画
	hbmp_Minimap=(HBITMAP)LoadImage(NULL,"system\\minimap.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);//调入小地图（480*480像素）
	hbmp_PropIcon=(HBITMAP)LoadImage(NULL,"pic\\props.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);//调入物品小图
	greenPen.CreatePen(PS_SOLID,1,0x0000ff00);//初始化绿色画笔
	redPen.CreatePen(PS_SOLID,1,0x000000ff);//初始化红色画笔
	//-█/////////////////////////////////////////////////////////////////////////////
	//调入光标
	//+█/////////////////////////////////////////////////////////////////////////////
	cur0=AfxGetApp()->LoadStandardCursor(IDC_ARROW);
	cur1=LoadCursor(AfxGetInstanceHandle(),MAKEINTRESOURCE(IDC_CURSOR1));
	cur2=LoadCursor(AfxGetInstanceHandle(),MAKEINTRESOURCE(IDC_CURSOR2));
	cur3=LoadCursor(AfxGetInstanceHandle(),MAKEINTRESOURCE(IDC_CURSOR3));
	cur4=LoadCursor(AfxGetInstanceHandle(),MAKEINTRESOURCE(IDC_CURSOR4));
	cur5=LoadCursor(AfxGetInstanceHandle(),MAKEINTRESOURCE(IDC_CURSOR5));
	cur6=LoadCursor(AfxGetInstanceHandle(),MAKEINTRESOURCE(IDC_CURSOR6));
	m_HForbiden=LoadCursor(AfxGetInstanceHandle(),MAKEINTRESOURCE(IDC_CURSORA));
	//-█/////////////////////////////////////////////////////////////////////////////
	//显示控制
	//+█/////////////////////////////////////////////////////////////////////////////
	m_info_exit=1;
	m_info_map=0;
	m_info_next=0;
	m_info_prop1=0;
	m_info_prop2=0;
	m_info_prop3=0;
	m_info_prop4=0;
	disdialogst=0;
	resetblt();

	CString sSnapDir;
	sSnapDir.Format("%s%s", theApp.GetLocalPath(), "snapshot");
	if (CreateSnapshotFolder(sSnapDir)) {
		m_info_snapshot = 1;
	}
	else {
		MessageBox("创建 屏幕截图文件夹 失败！", "提示信息", MB_OK);
		m_info_snapshot = 0;
	}
	//-█/////////////////////////////////////////////////////////////////////////////
	return 0;
}

void CMainFrame::StopMidiPlay(void)
{
	if (m_pMidi) {
		m_pMidi->StopMidi();
	}
}

/////////////////////////////////////////////////////////////////////////////
void CMainFrame::resetblt(void)//覆盖老位置的图像
{
	oldlx=0;
	oldly=0;
	oldx=0;
	oldy=0;
}
/////////////////////////////////////////////////////////////////////////////
void CMainFrame::blt(int lx,int ly,int x,int y,int msx,int msy,int sx,int sy,CDC *pdc)
{//l:左点坐标//xy:长宽//ms:源蒙版位置//s:源位置
	BuffDC.BitBlt(oldlx,oldly,oldx,oldy,&MapDC,oldlx,oldly,SRCCOPY);
	BuffDC.BitBlt(lx,ly,x,y,&MemDC,msx,msy,SRCAND);
	BuffDC.BitBlt(lx,ly,x,y,&MemDC,sx,sy,SRCINVERT);
	pdc->BitBlt(oldlx,oldly,oldx,oldy,&BuffDC,oldlx,oldly,SRCCOPY);
	oldlx=lx;oldly=ly;oldx=x;oldy=y;
	pdc->BitBlt(lx,ly,x,y,&BuffDC,lx,ly,SRCCOPY);
}
//-█/////////////////////////////////////////////////////////////////////////////
//打开地图图片、地图矩阵和NPC矩阵
//+█/////////////////////////////////////////////////////////////////////////////
void CMainFrame::OpenMap(char *fname1,char *fname2,char *fname3)
{//fname1:地图图片；fname2:地图矩阵；fname3:NPC矩阵。该函数同时存留oldcurrent。
	strcpy(fname01,fname1);
	strcpy(fname02,fname2);
	strcpy(fname03,fname3);
#ifdef DEBUG
	CString wnd_title = "劫掠轩辕剑（复刻版）";
	wnd_title = wnd_title + "--"+ fname1 + "--" + fname2 + "--" + fname3;
	this->SetWindowText(wnd_title);
#endif //DEBUG
	oldcurrent.x=current.x;
	oldcurrent.y=current.y;
	hbmp_Mapcils=(HBITMAP)LoadImage(NULL,fname1,IMAGE_BITMAP,0,0,LR_LOADFROMFILE);
	CilDC.SelectObject(hbmp_Mapcils);
	DeleteObject(hbmp_Mapcils);
	hbmp_Mapcils = NULL;
	FILE *fp=fopen(fname2,"rb");
	char temp[80020];
	fread(temp,sizeof(char),80000,fp);
	fclose(fp);
	int k;
	int i;
	for(i=0;i<200;i++){
		for(int j=0;j<200;j++){
			k=((i*200+j)*2);
			map[i][j]=((temp[k]*100)+temp[k+1]);
		}
	}
	if(fname3!=NULL){
		FILE *fp=fopen(fname3,"rb");
		fread(temp,sizeof(char),80000,fp);
		fclose(fp);
		for(i=0;i<200;i++){
			for(int j=0;j<200;j++){
				k=((i*200+j)*2);
				npc[i][j]=((temp[k]*100)+temp[k+1]);
			}
		}
	}
	else{
		for(i=0;i<200;i++){
			for(int j=0;j<200;j++){
				npc[i][j]=0;
			}
		}
	}
	//初始化50个敌人
	//首先，把它们都清空
	CDC *pdc=GetDC();
	CPoint enemypoint;
	enemypoint.x=0;
	enemypoint.y=0;
	for ( i=0 ; i<50 ; i++ ){
		m_enemies[i]->Doinit(0,enemypoint);
	}
	//下面，开始初始化敌人
	int sn=1;
	for(int sx=0;sx<200;sx++){
		for(int sy=0;sy<200;sy++){
			if((npc[sx][sy]>0)&&(npc[sx][sy]<7)){
				if(sn>=50){break;}//如果超出了敌人的数量的上限，就提前退出
				enemypoint.x=sx;
				enemypoint.y=sy;
				m_enemies[sn]->Doinit(npc[sx][sy],enemypoint);
				sn++;
			}
		}
	}
	ReleaseDC(pdc);
	pdc = NULL;
}
//-█/////////////////////////////////////////////////////////////////////////////
//调入主角
//+█/////////////////////////////////////////////////////////////////////////////
void CMainFrame::LoadActor(char *fname)
{
	DeleteObject(hbmp_Actor);
	hbmp_Actor=(HBITMAP)LoadImage(NULL,fname,IMAGE_BITMAP,0,0,LR_LOADFROMFILE);
	ActorDC.SelectObject(hbmp_Actor);
}
//-█/////////////////////////////////////////////////////////////////////////////
//调入对话
//+█/////////////////////////////////////////////////////////////////////////////
void CMainFrame::LoadDialog(char *fname)
{
	//dialogtext[320][130]
	//一个dlg文件，最多320行，每行最多130个字符
	ZeroMemory(dialogtext, sizeof(dialogtext));

	char buffer[320*130];
	FILE *fp = fopen(fname,"r");
	int file_len = fread (buffer, 1, sizeof(buffer), fp);

	int line_index = 0;
	int i;
	int k = 0;
	for (i = 0; i < file_len; i++)
	{
		if ('\n' == buffer[i])
		{//windows下回车是由“0x0D、0x0A”构成的，fopen时以‘r’模式打开，会自动转成0x0A
			k = 0;
			line_index ++;
			if (line_index >= 320) {
				break;
			}
		}
		else {
			if (k < 130) {
				dialogtext[line_index][k] = buffer[i];
				k ++;
			}
			//如果某行内容超长，后面的东西就不要了
		}
	}

	fclose(fp);
}
//-█/////////////////////////////////////////////////////////////////////////////
//场景切换，上下和左右两种方向的展开和关闭
//+█/////////////////////////////////////////////////////////////////////////////
void CMainFrame::lopen(void)
{
	CDC *pdc=GetDC();
	RECT rect;
	rect.left=399;
	rect.top=0;
	rect.right=401;
	rect.bottom=480;
	for (int m=0;m<101;m++){
		pdc->BitBlt(rect.left,0,(rect.right-rect.left),480,
						&BuffDC,rect.left,0,SRCCOPY);
		rect.left-=4;
		rect.right+=4;
	}
	ReleaseDC(pdc);
	pdc = NULL;
}
//
void CMainFrame::lclose(void)
{
	CDC *pdc=GetDC();
	RECT rect;
	rect.left=1;
	rect.top=0;
	rect.right=799;
	rect.bottom=480;
	for (int m=0;m<101;m++){
		pdc->BitBlt(0,0,rect.left,480,&BuffDC,0,0,BLACKNESS);
		pdc->BitBlt(rect.right,0,(800-rect.right),480,&BuffDC,0,0,BLACKNESS);
		rect.left+=4;
		rect.right-=4;
	}
	BuffDC.BitBlt(0,0,800,480,&MapDC,0,0,BLACKNESS);
	//用于绘制“请稍候……”
	rect.left=0;
	rect.top=400;
	rect.right=799;
	rect.bottom=479;
	pdc->SetTextColor(0x0000ffff);//黄色
	pdc->SetBkMode(TRANSPARENT);//设置透明背景
	HANDLE hOldObj = pdc->SelectObject(Font);
	pdc->DrawText("请稍侯……",10,&rect,DT_CENTER);
	pdc->SelectObject(hOldObj);
	//
	ReleaseDC(pdc);
	pdc = NULL;
}
//
void CMainFrame::vopen(void)
{
	CDC *pdc=GetDC();
	RECT rect;
	rect.left=0;
	rect.top=239;
	rect.right=800;
	rect.bottom=240;
	for (int m=0;m<61;m++){
		pdc->BitBlt(0,rect.top,800,(rect.bottom-rect.top),
						&BuffDC,0,rect.top,SRCCOPY);
		rect.top-=4;
		rect.bottom+=4;
		Sleep(5);
	}
	ReleaseDC(pdc);
	pdc = NULL;
}
//
void CMainFrame::vclose(void)
{
	CDC *pdc=GetDC();
	RECT rect;
	rect.left=0;
	rect.top=1;
	rect.right=800;
	rect.bottom=479;
	for (int m=0;m<61;m++){
		pdc->BitBlt(0,0,800,rect.top,&BuffDC,0,0,BLACKNESS);
		pdc->BitBlt(0,rect.bottom,800,(480-rect.bottom),&BuffDC,0,0,BLACKNESS);
		rect.top+=4;
		rect.bottom-=4;
		Sleep(5);
	}
	BuffDC.BitBlt(0,0,800,480,&MapDC,0,0,BLACKNESS);
	//用于绘制“请稍候……”
	rect.left=0;
	rect.top=400;
	rect.right=799;
	rect.bottom=479;
	pdc->SetTextColor(0x0000ffff);//黄色
	pdc->SetBkMode(TRANSPARENT);//设置透明背景
	pdc->DrawText("请稍侯……",10,&rect,DT_CENTER);
	//
	ReleaseDC(pdc);
	pdc = NULL;
}
//
void CMainFrame::quake(void)
{
	CDC *pdc=GetDC();
	int a=0;
	int b=0;
	int i;
	for(i=0;i<10;i++){
		pdc->BitBlt(0,0,800,480,&BuffDC,a,b,SRCCOPY);
		Sleep(20);
		a=-16;b=-8;
		pdc->BitBlt(0,0,800,480,&BuffDC,a,b,SRCCOPY);
		Sleep(20);
		a=0;b=0;
		pdc->BitBlt(0,0,800,480,&BuffDC,a,b,SRCCOPY);
		Sleep(20);
		a=-16;b=8;
		pdc->BitBlt(0,0,800,480,&BuffDC,a,b,SRCCOPY);
		Sleep(20);
		a=0;b=0;
	}
	pdc->BitBlt(0,0,800,480,&BuffDC,0,0,SRCCOPY);
	ReleaseDC(pdc);
	pdc = NULL;
}
//-█/////////////////////////////////////////////////////////////////////////////
//游戏结束的处理
//+█/////////////////////////////////////////////////////////////////////////////
void CMainFrame::gameover(void)
{
	CDC *pdc=GetDC();
	int i;
	int j;
	StopActTimer();
	if(st==9){//如果是第八段，则调用特殊的补充段落
		Para8GameOver(pdc);
	}
	for(j=0;j<240;j++){//红屏
		for(i=0;i<400;i++){
			BuffDC.BitBlt((i*2),(j*2),1,1,&InfoDC,746,104,SRCCOPY);
			pdc->BitBlt((i*2),(j*2),1,1,&InfoDC,746,104,SRCCOPY);
		}
	}
	BuffDC.StretchBlt(200,190,400,100,&InfoDC,748,97,49,9,SRCAND);
	pdc->StretchBlt(200,190,400,100,&InfoDC,748,97,49,9,SRCAND);
	Dialog(9,0,0,0,0);
	ReleaseDC(pdc);
	pdc = NULL;
}
//-█/////////////////////////////////////////////////////////////////////////////
//游戏的升级的处理
//+█/////////////////////////////////////////////////////////////////////////////
void CMainFrame::levelup(void)
{
	CDC *pdc=GetDC();
	int i;
	PlayWave("snd//levelup.wav");
	BuffDC.BitBlt(350,170,100,23,&InfoDC,100,107,SRCAND);
	BuffDC.BitBlt(350,170,100,23,&InfoDC,0,107,SRCINVERT);
	for(i=0;i<50;i++){
		pdc->BitBlt((400-i),170,(i*2),23,&BuffDC,(400-i),170,SRCCOPY);
		Sleep(20);
	}
	Sleep(500);
	blood=200;
	level+=10;
	if(level>200){level=200;}//限制等级最高值
	score=0;
	RenewInfo(pdc);
	ReleaseDC(pdc);
	pdc = NULL;
}
//-█/////////////////////////////////////////////////////////////////////////////
//敌人的处理
//+█/////////////////////////////////////////////////////////////////////////////
void CMainFrame::processenemies(void)
{//扩充到50个敌人
	int i;
	for (i=0 ; i<50 ; i++ ){
		m_enemies[i]->Dodisplay(&BuffDC,map,npc,movest,current,&blood,&level);
	}
}
//-█/////////////////////////////////////////////////////////////////////////////
//释放所有图片占用的内存
//+█/////////////////////////////////////////////////////////////////////////////
void CMainFrame::deleteallbmpobjects(void)
{
	DeleteObject(hbmp_Photo0);
	DeleteObject(hbmp_Photo1);
	DeleteObject(hbmp_Photo2);
	DeleteObject(hbmp_Photo3);
	DeleteObject(hbmp_Photo4);
	DeleteObject(hbmp_Photo5);
	DeleteObject(hbmp_Photo6);
	DeleteObject(hbmp_Photo7);
	DeleteObject(hbmp_Photo8);
	DeleteObject(hbmp_Photo9);
	DeleteObject(hbmp_Prop0);
	DeleteObject(hbmp_Prop1);
	DeleteObject(hbmp_Prop2);
	DeleteObject(hbmp_Prop3);
	DeleteObject(hbmp_Prop4);
	DeleteObject(hbmp_Prop5);
	DeleteObject(hbmp_Prop6);
	DeleteObject(hbmp_Prop7);
	DeleteObject(hbmp_Prop8);
	DeleteObject(hbmp_Prop9);
	DeleteObject(hbmp_0);
	DeleteObject(hbmp_1);
	DeleteObject(hbmp_2);
	DeleteObject(hbmp_3);
	DeleteObject(hbmp_4);
	DeleteObject(hbmp_5);
	DeleteObject(hbmp_6);
	DeleteObject(hbmp_7);
	DeleteObject(hbmp_8);
	DeleteObject(hbmp_9);
	DeleteObject(hbmp_10);
	DeleteObject(hbmp_11);
	DeleteObject(hbmp_12);
	DeleteObject(hbmp_13);
	DeleteObject(hbmp_14);
	DeleteObject(hbmp_15);
	DeleteObject(hbmp_16);
	DeleteObject(hbmp_17);
	DeleteObject(hbmp_18);
	DeleteObject(hbmp_19);
	DeleteObject(hbmp_20);
	DeleteObject(hbmp_21);
	DeleteObject(hbmp_22);
	DeleteObject(hbmp_23);
	DeleteObject(hbmp_24);
	DeleteObject(hbmp_25);
	DeleteObject(hbmp_26);
	DeleteObject(hbmp_27);
	DeleteObject(hbmp_28);
	DeleteObject(hbmp_29);
}

//███████████████████████████████████████
void CMainFrame::OnPaint()
{
	CPaintDC dc(this);
	CDC* pDC = &dc;

	//程序的第一部分，显示设计局声明，不做其他的工作
	// 一次性的初始化标志
	static BOOL _bOneOff = TRUE ;
	if (_bOneOff)
	{//初始化数值

		bf.AlphaFormat = 0;
		bf.BlendFlags = 0;
		bf.BlendOp = AC_SRC_OVER;
		bf.SourceConstantAlpha = 0;

		MapDC.CreateCompatibleDC(pDC);
		BuffDC.CreateCompatibleDC(pDC);
		CilDC.CreateCompatibleDC(pDC);
		MemDC.CreateCompatibleDC(pDC);
		DialogDC.CreateCompatibleDC(pDC);
		MiniMapDC.CreateCompatibleDC(pDC);
		BombDC.CreateCompatibleDC(pDC);
		InfoDC.CreateCompatibleDC(pDC);
		PropIconDC.CreateCompatibleDC(pDC);
		ActorDC.CreateCompatibleDC(pDC);

#ifdef DEBUG
		HBITMAP hbmp=(HBITMAP)LoadImage(NULL,"system\\codemap.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);
		EvtDC.CreateCompatibleDC(pDC);
		EvtDC.SelectObject(hbmp);
#endif //DEBUG

		bmp_MapLayer.CreateCompatibleBitmap(pDC, 1600, 480);//主地图后台缓冲区
		bmp_BufLayer.CreateCompatibleBitmap(pDC, 800, 480);//游戏界面的后台缓冲
		bmp_Dialog.CreateCompatibleBitmap(pDC, 616, 256);//对话框部分的缓冲
		MapDC.SelectObject(&bmp_MapLayer);
		BuffDC.SelectObject(&bmp_BufLayer);
		DialogDC.SelectObject(&bmp_Dialog);
		MiniMapDC.SelectObject(hbmp_Minimap);
		BombDC.SelectObject(&bmp_Bomb);
		PropIconDC.SelectObject(hbmp_PropIcon);
		InfoDC.SelectObject(&bmp_Info);
		Font=CreateFont(16,0,0,0,400,FALSE,FALSE,0,
			GB2312_CHARSET,OUT_DEFAULT_PRECIS,CLIP_DEFAULT_PRECIS,DEFAULT_QUALITY,DEFAULT_PITCH|FF_SWISS,"宋体");
		(DialogDC.SelectObject(Font));

		//以下一段程序用于显示设计局声明
		RECT rect;
		int m;
		MemDC.SelectObject(&bmp_start);
		PlayWave("snd//start.wav");
		pDC->BitBlt(0,0,800,567,&MemDC,0,0,BLACKNESS);
		//
		GetClientRect(&rect);
		rect.left=(rect.right-rect.left)/2-100;
		rect.top=(rect.bottom-rect.top)/2-20;
		rect.right=rect.left+202;
		rect.bottom=rect.top+42;
		//
		for (m=0;m<20;m++){
			for(int b=0;b<5;b++){
				pDC->StretchBlt(rect.left,rect.top,(rect.right-rect.left),(rect.bottom-rect.top),
								&MemDC,0,0,400,80,SRCCOPY);
				rect.left-=1;
				rect.right+=1;
				GameSleep(10);
			}
			rect.top-=1;
			rect.bottom+=1;
		}
		Sleep(3000);
		//设计局声明到此为止
		//初始化敌人所用的图片设备
		CEnemy::initStaticData(pDC);

		//铺设开始画面
		MemDC.SelectObject(&bmp_title);
		BuffDC.BitBlt(0,0,800,480,&MemDC,0,0,SRCCOPY);
		//信息条的显示
		RenewInfo(pDC);

		lopen();
		//开始画面到此结束
		//显示开始画面，给用户一个读取进度的选择
		Sleep(1000);
		Dialog(9,0,0,0,0);
		//显示选择信息的界面到此结束
		_bOneOff = FALSE;
		st = 1;
	}
	//程序第一部分结束
	//程序的其他部分，重绘屏幕即可。
	else if(st!=0){
		renewscreen(pDC);
	}
}

//-█/////////////////////////////////////////////////////////////////////////////
void CMainFrame::renewscreen(CDC *pdc)
{
	pdc->BitBlt(0,0,800,480,&BuffDC,0,0,SRCCOPY);//将缓冲区推到屏幕上
	RenewInfo(pdc);//刷新消息栏
	if(m_info_map==2){//刷新小地图
		pdc->BitBlt(160,0,480,480,&MiniMapDC,0,0,SRCCOPY);//绘制小地图
		CPen* hpen;
		hpen = (pdc->SelectObject(&greenPen));//选择绿色画笔
		DeleteObject (hpen);
		int ax=160+base_x+(current.x/2);//定义焦点坐标
		int ay=base_y+(current.y/2);
		pdc->MoveTo(160,ay);
		pdc->LineTo(ax-3,ay);//绘制横线
		pdc->MoveTo(ax+3,ay);
		pdc->LineTo(640,ay);//绘制横线
		pdc->MoveTo(ax,0);
		pdc->LineTo(ax,ay-3);//绘制纵线
		pdc->MoveTo(ax,ay+3);
		pdc->LineTo(ax,480);//绘制纵线
		hpen = (pdc->SelectObject(&redPen));//选择红色画笔
		DeleteObject (hpen);
		hpen = NULL;
		pdc->Ellipse(ax-3,ay-3,ax+4,ay+4);//绘制小圆圈
	}
	if(disdialogst==1){//刷新对话框
		pdc->BitBlt(91,223,616,256,&DialogDC,0,0,SRCCOPY);
	}
}
//███████████████████████████████████████
//███████████████████████████████████████
//█以下函数是关于鼠标消息的██████████████████████████
//███████████████████████████████████████
//+█/////////////////////////////////////////////////////////////////////////////
void CMainFrame::OnMouseMove(UINT nFlags, CPoint point) 
{
	m_mouse_pointer = point;

	CDC *pdc=GetDC();
	RenewInfoButtons(pdc);
	ReleaseDC(pdc);
	
	if(point.y>480) {
		::SetCursor(cur0);//采用箭头光标
	}

	//m_info_st的值  100：射击态；10：战斗态；9：对话态；8：自由态；7：系统态
	//射击态、战斗态、自由态，随着鼠标的位置转动Hero的朝向
	if((m_info_st==100)||(m_info_st==10)||(m_info_st==8)){

		//ShootingStatus
		// 1：左上
		// 2: 上
		// 3: 右上
		// 4: 左
		// 5: 默认左上
		// 6: 右
		// 7: 左下
		// 8: 下
		// 9: 右下

		if((point.x>383)&&(point.x<415)&&(point.y>223)&&(point.y<255)){
			//中间位置，状态不变
			ShootingStatus=5;//禁止开枪
			m_mouse_azimuth= AZIMUTH_DOWN;//默认面朝玩家
		}
		else{

			if((point.x<400)&&(point.y<208)){//左上区
				int dx=(400-point.x);
				int dy=(208-point.y);
				if((dx-dy)>dy){
					ShootingStatus=4;	m_mouse_azimuth= AZIMUTH_LEFT;}
				else if((dy-dx)>dx){
					ShootingStatus=2;	m_mouse_azimuth= AZIMUTH_UP;}
				else{
					ShootingStatus=1;	m_mouse_azimuth= AZIMUTH_UP;}
			}
			if((point.x>399)&&(point.y<208)){//右上区
				int dx=(point.x-399);
				int dy=(208-point.y);
				if((dx-dy)>dy){
					ShootingStatus=6;	m_mouse_azimuth= AZIMUTH_RIGHT;}
				else if((dy-dx)>dx){
					ShootingStatus=2;	m_mouse_azimuth= AZIMUTH_UP;}
				else{
					ShootingStatus=3;	m_mouse_azimuth= AZIMUTH_UP;}
			}
			if((point.x<400)&&(point.y>207)){//左下区
				int dx=(400-point.x);
				int dy=(point.y-207);
				if((dx-dy)>dy){
					ShootingStatus=4;	m_mouse_azimuth= AZIMUTH_LEFT;}
				else if((dy-dx)>dx){
					ShootingStatus=8;	m_mouse_azimuth= AZIMUTH_DOWN;}
				else{
					ShootingStatus=7;	m_mouse_azimuth= AZIMUTH_DOWN;}
			}
			if((point.x>399)&&(point.y>207)){//右下区
				int dx=(point.x-399);
				int dy=(point.y-207);
				if((dx-dy)>dy){
					ShootingStatus=6;	m_mouse_azimuth= AZIMUTH_RIGHT;}
				else if((dy-dx)>dx){
					ShootingStatus=8;	m_mouse_azimuth= AZIMUTH_DOWN;}
				else{
					ShootingStatus=9;	m_mouse_azimuth= AZIMUTH_DOWN;}
			}
		}
	}
	// 射击态还要修改鼠标光标
	if (m_info_st==100) {
		if((point.x>383)&&(point.x<415)&&(point.y>223)&&(point.y<255)){
			//中间，5号位置，禁止开枪
			::SetCursor(m_HForbiden);
		}
		else if(point.y<=480) {
			::SetCursor(cur1);//采用准星光标
		}
	}
	else {
		::SetCursor(cur0);//采用箭头光标
	}
}
//-█/////////////////////////////////////////////////////////////////////////////
//用于显示不同方向射击图片所用到的子程序
void CMainFrame::displayFightingHero(int sst,int snum,CDC* pDC)
{
	int sx,sy;
	switch(sst){
	case 1:sy=480;sx=0;break;//左上
	case 2:sy=624;sx=0;break;//上
	case 3:sy=480;sx=48;break;//右上
	case 4:sy=528;sx=0;break;//左
	case 5:sy=480;sx=0;break;//默认左上
	case 6:sy=528;sx=48;break;//右
	case 7:sy=576;sx=0;break;//左下
	case 8:sy=624;sx=48;break;//下
	case 9:sy=576;sx=48;break;//右下
	default:sy=480;sx=0;
	}
	if(snum==1){
		sy+=192;}
	pDC->BitBlt(384,208,48,48,&MapDC,384,208,SRCCOPY);
	pDC->BitBlt(384,208,48,48,&ActorDC,(sx+96),sy,SRCAND);
	pDC->BitBlt(384,208,48,48,&ActorDC,sx,sy,SRCINVERT);
}
//███████████████████████████████████████
void CMainFrame::OnLButtonDown(UINT nFlags, CPoint point) 
{//鼠标按键是否被按下，改在ActProc中判断
	if (2==m_info_map) {//关闭小地图
		SetActTimer();
		m_info_map=1;
		return;
	}

	CDC *pdc=GetDC();

	//处理其它各种状态的显示
	if((point.x>30)&&(point.x<90)&&(point.y>528)&&(point.y<547)){//点击exit区
			
		if (ST_DIALOG_NORMAL_EXIT==st_dialog)
		{//剧情规定的退出位置，不提示直接退出
			StopMidiPlay();//关闭MIDI设备
			::PostQuitMessage(WM_QUIT);//退出程序
		}
		else if (IDOK == MessageBox("您……确定要退出游戏吗？", "确认退出", MB_OKCANCEL)) {
			StopMidiPlay();//关闭MIDI设备
			::PostQuitMessage(WM_QUIT);//退出程序
		}
		else {
			m_info_st=m_oldinfo_st;
			if((m_info_st==100)||(m_info_st==10)||(m_info_st==8)){//恢复定时器
				SetActTimer();
			}
			if(m_oldinfo_next==1){
				m_info_map=m_oldinfo_map;
				m_info_next=m_oldinfo_next;
				Dialog(m_olddialog[0],m_olddialog[1],m_olddialog[2],m_olddialog[3],m_olddialog[4]);
			}
		}
	}
	if((point.x>30)&&(point.x<90)&&(point.y>500)&&(point.y<519)
		&&(m_info_next==0))
	{//在运动或者战斗中点击map区。对话时点击无效。
		if((m_info_map==1||m_info_map==2)&&
			(m_info_st==8||m_info_st==10||m_info_st==100)){
			pdc->BitBlt(30,500,60,19,&InfoDC,430,87,SRCCOPY);
			PlayWave("snd//type.wav");
			Sleep(300);
			
			if(m_info_map==1){//显示地图
				StopActTimer();
				m_info_map=2;//表示地图已经处于打开状态
				m_info_next=0;//不允许点击NEXT
			}
			else if(m_info_map=2){//隐藏地图
				SetActTimer();
				m_info_map=1;
				m_info_exit=1;//允许退出
			}
			renewscreen(pdc);//擦去或者显示小地图
		}
	}
	if(m_info_next==1){
		CRect rcNextBtn(640,500,770,519);
		CRect rcDialog(220,350,580,480);
		if (PtInRect(&rcNextBtn, point) ||//点击next区
			PtInRect(&rcDialog, point))
		{
			pdc->BitBlt(640,500,130,19,&InfoDC,490,87,SRCCOPY);
			PlayWave("snd//type.wav");
			Sleep(300);
			m_info_next=2;//表示点击过了
			disdialogst=0;
			renewscreen(pdc);//擦去对话框
		}
	}
	if((point.x>710)&&(point.x<770)&&(point.y>528)&&(point.y<547)){//snapshot
		if((m_info_snapshot==1)){
			pdc->BitBlt(710,528,60,19,&InfoDC,680,87,SRCCOPY);

			CTime tmNow = CTime::GetCurrentTime();
			CString strTime = tmNow.Format("%Y-%m-%d_%H-%M-%S");
			CString sSnapFile;
			sSnapFile.Format("%s%s\\%s%s", theApp.GetLocalPath(), "snapshot", strTime, ".png");

			//保存为Png文件
			CImage png;
			png.Create(800, 480, 24);
			CDC dcPng;
			dcPng.Attach(png.GetDC());
			dcPng.BitBlt(0, 0, 800, 480, pdc, 0, 0, SRCCOPY);
			png.Save(sSnapFile);
			dcPng.Detach();
			png.ReleaseDC();
			png.Destroy();

			PlayWave("snd//type.wav");
			MessageBox("屏幕截图已经保存在 snapshot 目录下");
		}
	}
	/////////////////////////////////////////////////////////////////////////////
	//对系统状态的处理
	if(m_info_st==6){//游戏从存盘位置开始
		InitGame();
	}
	/////////////////////////////////////////////////////////////////////////////
	//对其他事件的处理
	if((m_info_st==8)||(m_info_st==9)||(m_info_st==10)||(m_info_st==100)){
		if(m_info_next==2){
			m_info_next=0;
			OnInfo(1);
		}
	}
	/////////////////////////////////////////////////////////////////////////////
	ReleaseDC(pdc);
}
//███████████████████████████████████████
void CMainFrame::OnRButtonDown(UINT nFlags, CPoint point) 
{//鼠标按键是否被按下，改在ActProc中判断
	if (2==m_info_map) {//关闭小地图
		SetActTimer();
		m_info_map=1;
		return;
	}

	CDC *pdc=GetDC();

	//右键射击
	if (m_info_st==100) {//处理射击
		if((point.x>383)&&(point.x<415)&&(point.y>223)&&(point.y<255)){//中间不开枪
			::SetCursor(m_HForbiden);
		}
		else if(bullet>0){//如果有子弹，则允许射击。
			::SetCursor(cur1);//采用准星光标
			displayFightingHero(ShootingStatus,1,pdc);//显示不同方向射击图片
			
			if(!super_bullet)
			{//普通子弹
				PlayWave("snd//shoot.wav");
				Sleep(30);
				::SetCursor(cur2);
				Sleep(30);
				::SetCursor(cur3);
				Sleep(30);
				::SetCursor(cur4);
				Sleep(30);
				::SetCursor(cur5);
				Sleep(30);
				::SetCursor(cur6);
				Sleep(30);
				::SetCursor(cur1);
			}
			else
			{//超级子弹
				PlayWave("snd\\bomb2.wav");
				int i;
				for(i=0;i<10;i++){
					pdc->BitBlt(point.x-35,point.y-35,70,70,&BuffDC,point.x-35,point.y-35,SRCCOPY);
					pdc->StretchBlt(point.x-35,point.y-35,70,70,&BombDC,i*54,50,54,50,SRCAND);
					pdc->StretchBlt(point.x-35,point.y-35,70,70,&BombDC,i*54,0,54,50,SRCINVERT);
					Sleep(40);
				}
			}

			bullet--;
			RenewInfo(pdc);//刷新子弹
			int power;
			GetPower(1,&power);//计算伤害，1表示用枪
			OnShoot(power,point);//射击，扩大打击范围
			OnShoot(power,CPoint(point.x-32, point.y));//扩大打击范围
			OnShoot(power,CPoint(point.x+32, point.y));//扩大打击范围
			OnShoot(power,CPoint(point.x, point.y-32));//扩大打击范围
			OnShoot(power,CPoint(point.x, point.y+32));//扩大打击范围
			if (super_bullet) {//超级子弹，进一步扩大打击范围
				OnShoot(power,CPoint(point.x-32, point.y-32));
				OnShoot(power,CPoint(point.x+32, point.y+32));
				OnShoot(power,CPoint(point.x+32, point.y-32));
				OnShoot(power,CPoint(point.x-32, point.y+32));
				OnShoot(power,CPoint(point.x-64, point.y+16));
				OnShoot(power,CPoint(point.x-64, point.y-16));
				OnShoot(power,CPoint(point.x+64, point.y+16));
				OnShoot(power,CPoint(point.x+64, point.y-16));
				OnShoot(power,CPoint(point.x+16, point.y-64));
				OnShoot(power,CPoint(point.x-16, point.y-64));
				OnShoot(power,CPoint(point.x+16, point.y+64));
				OnShoot(power,CPoint(point.x-16, point.y+64));
			}
		}
		else if(bullet==0){//如果子弹用尽，则换成普通战斗态
			m_info_st=10;
		}
	}
}
//███████████████████████████████████████
void CMainFrame::OnLButtonUp(UINT nFlags, CPoint point) 
{//鼠标按键是否被按下，改在ActProc中判断
	////以下一段程序用于修正光标跳变问题
	//if(m_info_st==100){
	//	if((point.x>383)&&(point.x<415)&&(point.y>223)&&(point.y<255)){//中间不开枪
	//		::SetCursor(m_HForbiden);//采用禁止光标
	//	}
	//	else{
	//		::SetCursor(cur1);//采用准星光标
	//	}
	//}
}
void CMainFrame::OnRButtonUp(UINT nFlags, CPoint point) 
{//鼠标按键是否被按下，改在ActProc中判断
	////以下一段程序用于修正光标跳变问题
	//if(m_info_st==100){
	//	::SetCursor(cur1);//采用准星光标
	//}
}
//███████████████████████████████████████
//███████████████████████████████████████
//█ 处理键盘操作 █████████████████████████
//███████████████████████████████████████
BOOL CMainFrame::GoUp() 
{
	if(((m_info_st==8)||(m_info_st==10)||(m_info_st==100))&&(azimuth<5)){
		azimuth= AZIMUTH_UP;
		//地图格子，1-200范围是可以行走的；NPC格子，40以下是给NPC移动占位用的
		if((map[(current.x)][(current.y-1)]<201)&&(map[(current.x)][(current.y-1)]>0)
		&&((npc[(current.x)][(current.y-1)]<10)||(npc[(current.x)][(current.y-1)]==810))){
			azimuth=5;
			npc[oldcurrent.x][oldcurrent.y]=0;
			npc[current.x][current.y]=810;
			npc[current.x][(current.y-1)]=800;
		}
		else {
			if(npc[(current.x)][(current.y-1)]>40){
				CDC *pdc=GetDC();
				BuffDC.BitBlt(384,207,32,48,&MapDC,384,207,SRCCOPY);//拷贝背景
				BuffDC.BitBlt(384,207,32,48,&ActorDC,96,96,SRCAND);//拷贝蒙版
				BuffDC.BitBlt(384,207,32,48,&ActorDC,0,96,SRCINVERT);//拷贝图像
				pdc->BitBlt(384,207,32,48,&BuffDC,384,207,SRCCOPY);//写屏
				ReleaseDC(pdc);
				pdc = NULL;
				//以上四句用于绘制静止人物
				PostMessage(ID_ACCIDENT,(npc[(current.x)][(current.y-1)]),0);
			}
			else {
				//表示撞墙了
				return FALSE;
			}
		}
	}
	return TRUE;
}
/////////////////////////////////////////////////
BOOL CMainFrame::GoDown() 
{
	if(((m_info_st==8)||(m_info_st==10)||(m_info_st==100))&&(azimuth<5)){
		azimuth= AZIMUTH_DOWN;
		//地图格子，1-200范围是可以行走的；NPC格子，40以下是给NPC移动占位用的
		if((map[(current.x)][(current.y+1)]<201)&&(map[(current.x)][(current.y+1)]>0)
		&&((npc[(current.x)][(current.y+1)]<10)||(npc[(current.x)][(current.y+1)]==810))){
			azimuth=6;
			npc[oldcurrent.x][oldcurrent.y]=0;
			npc[current.x][current.y]=810;
			npc[current.x][(current.y+1)]=800;
		}
		else {
			if(npc[(current.x)][(current.y+1)]>40){
				CDC *pdc=GetDC();
				BuffDC.BitBlt(384,207,32,48,&MapDC,384,207,SRCCOPY);//拷贝背景
				BuffDC.BitBlt(384,207,32,48,&ActorDC,96,0,SRCAND);//拷贝蒙版
				BuffDC.BitBlt(384,207,32,48,&ActorDC,0,0,SRCINVERT);//拷贝图像
				pdc->BitBlt(384,207,32,48,&BuffDC,384,207,SRCCOPY);//写屏
				ReleaseDC(pdc);
				pdc = NULL;
				//以上四句用于绘制静止人物
				PostMessage(ID_ACCIDENT,(npc[(current.x)][(current.y+1)]),0);
			}
			else {
				//表示撞墙了
				return FALSE;
			}
		}
	}
	return TRUE;
}
/////////////////////////////////////////////////
BOOL CMainFrame::GoLeft() 
{
	if(((m_info_st==8)||(m_info_st==10)||(m_info_st==100))&&(azimuth<5)){
		azimuth= AZIMUTH_LEFT;
		//地图格子，1-200范围是可以行走的；NPC格子，40以下是给NPC移动占位用的
		if((map[(current.x-1)][(current.y)]<201)&&(map[(current.x-1)][(current.y)]>0)
		&&((npc[(current.x-1)][(current.y)]<10)||(npc[(current.x-1)][(current.y)]==810))){
			azimuth=7;
			npc[oldcurrent.x][oldcurrent.y]=0;
			npc[current.x][current.y]=810;
			npc[(current.x-1)][current.y]=800;
		}
		else {
			if(npc[(current.x-1)][(current.y)]>40){
				CDC *pdc=GetDC();
				BuffDC.BitBlt(384,207,32,48,&MapDC,384,207,SRCCOPY);//拷贝背景
				BuffDC.BitBlt(384,207,32,48,&ActorDC,96,48,SRCAND);//拷贝蒙版
				BuffDC.BitBlt(384,207,32,48,&ActorDC,0,48,SRCINVERT);//拷贝图像
				pdc->BitBlt(384,207,32,48,&BuffDC,384,207,SRCCOPY);//写屏
				ReleaseDC(pdc);
				pdc = NULL;
				//以上四句用于绘制静止人物
				PostMessage(ID_ACCIDENT,(npc[(current.x-1)][(current.y)]),0);
			}
			else {
				//表示撞墙了
				return FALSE;
			}
		}
	}
	return TRUE;
}
/////////////////////////////////////////////////
BOOL CMainFrame::GoRight() 
{
	if(((m_info_st==8)||(m_info_st==10)||(m_info_st==100))&&(azimuth<5)){
		azimuth= AZIMUTH_RIGHT;
		//地图格子，1-200范围是可以行走的；NPC格子，40以下是给NPC移动占位用的
		if((map[(current.x+1)][(current.y)]<201)&&(map[(current.x+1)][(current.y)]>0)
		&&((npc[(current.x+1)][(current.y)]<10)||(npc[(current.x+1)][(current.y)]==810))){
			azimuth=8;
			npc[oldcurrent.x][oldcurrent.y]=0;
			npc[current.x][current.y]=810;
			npc[(current.x+1)][current.y]=800;
		}
		else {
			if(npc[(current.x+1)][(current.y)]>40){
				CDC *pdc=GetDC();
				BuffDC.BitBlt(384,207,32,48,&MapDC,384,207,SRCCOPY);//拷贝背景
				BuffDC.BitBlt(384,207,32,48,&ActorDC,96,144,SRCAND);//拷贝蒙版
				BuffDC.BitBlt(384,207,32,48,&ActorDC,0,144,SRCINVERT);//拷贝图像
				pdc->BitBlt(384,207,32,48,&BuffDC,384,207,SRCCOPY);//写屏
				ReleaseDC(pdc);
				pdc = NULL;
				//以上四句用于绘制静止人物
				PostMessage(ID_ACCIDENT,(npc[(current.x+1)][(current.y)]),0);
			}
			else {
				//表示撞墙了
				return FALSE;
			}
		}
	}
	return TRUE;
}
/////////////////////////////////////////////////
void CMainFrame::Fight() 
{
	int power;
	CPoint point;
	if(m_info_st==10){
		if(m_info_prop2==8){//短刀攻击
			GetPower(3,&power);//计算伤害，3短刀攻击
			switch(azimuth){
			case AZIMUTH_UP:
				point.x=399;point.y=207;azimuth=13;
				OnShoot(power,point);//射击
				point.x=367;
				OnShoot(power,point);//射击
				point.x=431;
				OnShoot(power,point);//射击
				point.x=399;point.y=175;//扩大打击范围
				OnShoot(power,point);//射击
				point.x=367;
				OnShoot(power,point);//射击
				point.x=431;
				OnShoot(power,point);//射击
				break;
			case AZIMUTH_DOWN:
				point.x=399;point.y=271;azimuth=14;
				OnShoot(power,point);//射击
				point.x=367;
				OnShoot(power,point);//射击
				point.x=431;
				OnShoot(power,point);//射击
				point.x=399;point.y=303;//扩大打击范围
				OnShoot(power,point);//射击
				point.x=367;
				OnShoot(power,point);//射击
				point.x=431;
				OnShoot(power,point);//射击
				break;
			case AZIMUTH_LEFT:
				point.x=367;point.y=239;azimuth=15;
				OnShoot(power,point);//射击
				point.y=207;
				OnShoot(power,point);//射击
				point.y=271;
				OnShoot(power,point);//射击
				point.x=335;point.y=239;//扩大打击范围
				OnShoot(power,point);//射击
				point.y=207;
				OnShoot(power,point);//射击
				point.y=271;
				OnShoot(power,point);//射击
				break;
			case AZIMUTH_RIGHT:
				point.x=431;point.y=239;azimuth=16;
				OnShoot(power,point);//射击
				point.y=207;
				OnShoot(power,point);//射击
				point.y=271;
				OnShoot(power,point);//射击
				point.x=463;point.y=239;//扩大打击范围
				OnShoot(power,point);//射击
				point.y=207;
				OnShoot(power,point);//射击
				point.y=271;
				OnShoot(power,point);//射击
				break;
			}
		}
		else{//拳头攻击
			GetPower(2,&power);//计算伤害，2表示拳头
			switch(azimuth){
			case AZIMUTH_UP:
				azimuth=9;
				point.x=399;point.y=207;
				OnShoot(power,point);//射击
				point.x=367;
				OnShoot(power,point);//射击
				point.x=431;
				OnShoot(power,point);//射击
				break;
			case AZIMUTH_DOWN:
				azimuth=10;
				point.x=399;point.y=271;
				OnShoot(power,point);//射击
				point.x=367;
				OnShoot(power,point);//射击
				point.x=431;
				OnShoot(power,point);//射击
				break;
			case AZIMUTH_LEFT:
				azimuth=11;
				point.x=367;point.y=239;
				OnShoot(power,point);//射击
				point.y=207;
				OnShoot(power,point);//射击
				point.y=271;
				OnShoot(power,point);//射击
				break;
			case AZIMUTH_RIGHT:
				azimuth=12;
				point.x=431;point.y=239;
				OnShoot(power,point);//射击
				point.y=207;
				OnShoot(power,point);//射击
				point.y=271;
				OnShoot(power,point);//射击
				break;
			}
		}
	}

	//战斗之后，把Hero周围的行走预占位（800）全清掉
	if(800==npc[current.x+1][current.y+1]) npc[current.x+1][current.y+1]=0;
	if(800==npc[current.x+1][current.y])   npc[current.x+1][current.y]=0;
	if(800==npc[current.x+1][current.y-1]) npc[current.x+1][current.y-1]=0;
	if(800==npc[current.x][current.y+1])   npc[current.x][current.y+1]=0;
	if(800==npc[current.x][current.y-1])   npc[current.x][current.y-1]=0;
	if(800==npc[current.x-1][current.y+1]) npc[current.x-1][current.y+1]=0;
	if(800==npc[current.x-1][current.y])   npc[current.x-1][current.y]=0;
	if(800==npc[current.x-1][current.y-1]) npc[current.x-1][current.y-1]=0;
}
/////////////////////////////////////////////////
//███████████████████████████████████████
//███████████████████████████████████████
//以下是用到的子程序
void CMainFrame::MoveActor(void)
{
	if(movest==0){
		switch(azimuth){
		case 5:movest=1;break;
		case 6:movest=5;break;
		case 7:movest=9;break;
		case 8:movest=13;break;
		case 9:movest=19;break;
		case 10:movest=23;break;
		case 11:movest=27;break;
		case 12:movest=31;break;
		case 13:movest=17;break;
		case 14:movest=21;break;
		case 15:movest=25;break;
		case 16:movest=29;break;
		}
	}
	if(movest<17){//如果在行走，就让他走两步
		movest++;
	}
	if(azimuth<9){DrawMap();}//按照当前的状态绘制地图
	BuffDC.BitBlt(0,0,800,480,&MapDC,0,0,SRCCOPY);//将地图复制到缓冲页面
	if((m_info_st==10)||(m_info_st==100)){displayenemies();}//显示各种敌人
	if((movest==4)||(movest==8)||(movest==12)||(movest==16)||
		(movest==20)||(movest==24)||(movest==28)||(movest==32)){
		switch(azimuth){//在坐标上实现当前位置的改变
		case 5:oldcurrent.x=current.x;oldcurrent.y=current.y;current.y--;azimuth= AZIMUTH_UP;break;
		case 6:oldcurrent.x=current.x;oldcurrent.y=current.y;current.y++;azimuth= AZIMUTH_DOWN;break;
		case 7:oldcurrent.x=current.x;oldcurrent.y=current.y;current.x--;azimuth= AZIMUTH_LEFT;break;
		case 8:oldcurrent.x=current.x;oldcurrent.y=current.y;current.x++;azimuth= AZIMUTH_RIGHT;break;
		case 9:azimuth= AZIMUTH_UP;movest=0;break;
		case 10:azimuth= AZIMUTH_DOWN;movest=0;break;
		case 11:azimuth= AZIMUTH_LEFT;movest=0;break;
		case 12:azimuth= AZIMUTH_RIGHT;movest=0;break;
		case 13:azimuth= AZIMUTH_UP;movest=0;break;
		case 14:azimuth= AZIMUTH_DOWN;movest=0;break;
		case 15:azimuth= AZIMUTH_LEFT;movest=0;break;
		case 16:azimuth= AZIMUTH_RIGHT;movest=0;break;
		}
	}
	if((azimuth>8)&&(azimuth>4)&&(azimuth!=19)&&(azimuth!=23)&&(azimuth!=27)&&(azimuth!=31)){
		ActActor(1);}//绘制动作
	else{
 		DrawActor(1);}//按照当前的状态绘制主角
	if(movest!=0){
		movest++;//进入下一个动作
	}
	if((movest==5)||(movest==9)||(movest==13)||(movest==17)||
		(movest==21)||(movest==25)||(movest==29)||(movest==33)){
		movest=0;
	}
}
//新的运动函数结束。
/////////////////////////////////////////////////
void CMainFrame::ActActor(int type)//绘制动作的主角
{
	int x;
	int y;
	int sx;
	int sw;
	switch(movest){
		case 17:x=0;y=432;sx=376;sw=48;break;
		case 18:x=0;y=384;sx=376;sw=48;break;
		case 19:x=0;y=336;sx=376;sw=48;break;
		case 21:x=48;y=432;sx=376;sw=48;break;
		case 22:x=48;y=384;sx=376;sw=48;break;
		case 23:x=48;y=336;sx=376;sw=48;break;
		case 25:x=0;y=288;sx=368;sw=48;break;
		case 26:x=0;y=240;sx=368;sw=48;break;
		case 27:x=0;y=192;sx=368;sw=48;break;
		case 29:x=48;y=288;sx=384;sw=48;break;
		case 30:x=48;y=240;sx=384;sw=48;break;
		case 31:x=48;y=192;sx=384;sw=48;break;
	}
//	BuffDC.BitBlt(sx,207,sw,48,&MapDC,sx,207,SRCCOPY);//拷贝背景
	BuffDC.BitBlt(sx,207,sw,48,&ActorDC,(x+96),y,SRCAND);//拷贝蒙版
	BuffDC.BitBlt(sx,207,sw,48,&ActorDC,x,y,SRCINVERT);//拷贝图像
	if(type==0){
		CDC *pdc=GetDC();
		pdc->BitBlt(sx,207,sw,48,&BuffDC,384,207,SRCCOPY);//写屏
		ReleaseDC(pdc);
		pdc = NULL;
	}
}
/////////////////////////////////////////////////
void CMainFrame::DrawActor(int type)//绘制静止的主角(一次)type:0:写屏
{
	int x;
	int y;
	switch(movest){
		case 0:
			switch(azimuth){
			case 1:x=0;y=96;break;
			case 2:x=0;y=0;break;
			case 3:x=0;y=48;break;
			case 4:x=0;y=144;break;
			};break;
		case 2:x=64;y=96;break;
		case 4:x=32;y=96;break;
		case 6:x=64;y=0;break;
		case 8:x=32;y=0;break;
		case 10:x=64;y=48;break;
		case 12:x=32;y=48;break;
		case 14:x=64;y=144;break;
		case 16:x=32;y=144;break;

		default://意外的值
			return;
	}
//	BuffDC.BitBlt(384,207,32,48,&BuffDC,384,207,SRCCOPY);//拷贝背景
	BuffDC.BitBlt(384,223,32,32,&MapDC,384,223,SRCCOPY);//拷贝背景
	BuffDC.BitBlt(384,207,32,48,&ActorDC,(x+96),y,SRCAND);//拷贝蒙版
	BuffDC.BitBlt(384,207,32,48,&ActorDC,x,y,SRCINVERT);//拷贝图像
	if(type==0){
		CDC *pdc=GetDC();
		pdc->BitBlt(384,207,32,48,&BuffDC,384,207,SRCCOPY);//写屏
		ReleaseDC(pdc);
		pdc = NULL;
	}
}
//███████████████████████████████████████
//███████████████████████████████████████
//█其他函数██████████████████████████████████
//███████████████████████████████████████
//以下程序是信息条的显示
//+█/////////////////////////////////////////////////////////////////////////////
void CMainFrame::RenewInfo(CDC *pdc)
{
	int i;
	pdc->BitBlt(0,480,800,87,&InfoDC,0,0,SRCCOPY);//显示背景
	pdc->StretchBlt(190,500,blood,9,&InfoDC,746,87,1,9,SRCCOPY);//显示生命
	pdc->StretchBlt(190,519,level,9,&InfoDC,773,87,1,9,SRCCOPY);//显示等级
	pdc->StretchBlt(190,519,score,9,&InfoDC,777,87,1,9,SRCCOPY);//显示分数
	//显示子弹
	int k=((bullet>65)?65:bullet);
	for(i=0;i<k;i++){
		pdc->BitBlt((190+(i*6)),535,6,19,&InfoDC,740,87,SRCCOPY);
	}
	//显示所有的按钮
	RenewInfoButtons(pdc);
	//显示物品
	if(m_info_prop1!=0){
		pdc->BitBlt(420,497,32,32,&PropIconDC,((m_info_prop1-1)*32),0,SRCCOPY);
	}
	if(m_info_prop2!=0){
		pdc->BitBlt(460,497,32,32,&PropIconDC,((m_info_prop2-1)*32),0,SRCCOPY);
	}
	if(m_info_prop3!=0){
		pdc->BitBlt(500,497,32,32,&PropIconDC,((m_info_prop3-1)*32),0,SRCCOPY);
	}
	if(m_info_prop4!=0){
		pdc->BitBlt(540,497,32,32,&PropIconDC,((m_info_prop4-1)*32),0,SRCCOPY);
	}
};

void CMainFrame::RenewInfoButtons(CDC *pdc)
{
	CPoint point(m_mouse_pointer);

	if(m_info_exit==1){//移过exit区
		pdc->BitBlt(30,528,60,19,&InfoDC,0,87,SRCCOPY);
		if((point.x>30)&&(point.x<90)&&(point.y>528)&&(point.y<547)){
			pdc->StretchBlt(31,529,58,17,&InfoDC,797,87,1,19,SRCAND);
		}
	}

	if((m_info_st==100)||(m_info_st==10)||(m_info_st==8)){
		if((m_info_map==1)||(m_info_map==2)){//移过map区
			pdc->BitBlt(30,500,60,19,&InfoDC,60,87,SRCCOPY);
			if((point.x>30)&&(point.x<90)&&(point.y>500)&&(point.y<519)){
				pdc->StretchBlt(31,501,58,17,&InfoDC,797,87,1,19,SRCAND);
			}
		}
	}

	if(m_info_next==1){//移过next区
		pdc->BitBlt(640,500,130,19,&InfoDC,120,87,SRCCOPY);
		if((point.x>640)&&(point.x<770)&&(point.y>500)&&(point.y<519)){
			pdc->StretchBlt(641,501,128,17,&InfoDC,797,87,1,19,SRCAND);
		}
	}

	if(m_info_snapshot==1) {//移过Snapshot按钮区
		pdc->BitBlt(710,528,60,19,&InfoDC,310,87,SRCCOPY);
		if((point.x>710)&&(point.x<770)&&(point.y>528)&&(point.y<547)){
			pdc->StretchBlt(711,529,58,17,&InfoDC,797,87,1,19,SRCAND);
		}
	}
}
//-█/////////////////////////////////////////////////////////////////////////////
//███████████████████████████████████████
//下面的函数是显示对话框和图片的
//+█/////////////////////////////////////////////////////////////////////////////
void CMainFrame::Dialog(int mode,int lface,int rface,int prop,int text)
{
	CDC *pdc=GetDC();
	int i;

	if(m_info_st==100){
	m_info_st=10;//返回战斗态
	::SetCursor(cur0);//采用箭头光标
	}
	StopActTimer();
	DialogDC.BitBlt(0,0,616,256,&BuffDC,91,223,SRCCOPY);//从主缓冲中复制出背景
	if((mode==10)){//解决对话重现的问题
		m_oldinfo_map=m_info_map;
		m_oldinfo_next=m_info_next;
	}
	else{
		m_olddialog[0]=mode;
		m_olddialog[1]=lface;
		m_olddialog[2]=rface;
		m_olddialog[3]=prop;
		m_olddialog[4]=text;
	}
	//////////////////////////////////////////////////////////////////
	//下面一段用于解析lface参数并进行相应的显示
	int k=(((lface%5)==0)?5:(lface%5));
	k*=128;
	if(lface!=0){//显示左侧的头像，0表示不显示
		if((lface>0)&&(lface<6)){
			MemDC.SelectObject(hbmp_Photo0);
		}
		if((lface>5)&&(lface<11)){
			MemDC.SelectObject(hbmp_Photo1);
		}
		if((lface>10)&&(lface<16)){
			MemDC.SelectObject(hbmp_Photo2);
		}
		if((lface>15)&&(lface<21)){
			MemDC.SelectObject(hbmp_Photo3);
		}
		if((lface>20)&&(lface<26)){
			MemDC.SelectObject(hbmp_Photo4);
		}
		if((lface>25)&&(lface<31)){
			MemDC.SelectObject(hbmp_Photo5);
		}
		if((lface>30)&&(lface<36)){
			MemDC.SelectObject(hbmp_Photo6);
		}
		if((lface>35)&&(lface<41)){
			MemDC.SelectObject(hbmp_Photo7);
		}
		if((lface>40)&&(lface<46)){
			MemDC.SelectObject(hbmp_Photo8);
		}
		if((lface>45)&&(lface<51)){
			MemDC.SelectObject(hbmp_Photo9);
		}
		DialogDC.BitBlt(0,128,128,128,&MemDC,0,0,SRCAND);//蒙版位图
		DialogDC.BitBlt(0,128,128,128,&MemDC,k,0,SRCINVERT);//图形位图
		pdc->BitBlt(91,351,128,128,&DialogDC,0,128,SRCCOPY);//送屏幕显示
	}
	//////////////////////////////////////////////////////////////////
	//下面一段用于解析rface参数并进行相应的显示
	k=(((rface%5)==0)?5:(rface%5));
	k*=128;
	if(rface!=0){//显示右侧的头像，0表示不显示
		if((rface>0)&&(rface<6)){
			MemDC.SelectObject(hbmp_Photo0);
		}
		if((rface>5)&&(rface<11)){
			MemDC.SelectObject(hbmp_Photo1);
		}
		if((rface>10)&&(rface<16)){
			MemDC.SelectObject(hbmp_Photo2);
		}
		if((rface>15)&&(rface<21)){
			MemDC.SelectObject(hbmp_Photo3);
		}
		if((rface>20)&&(rface<26)){
			MemDC.SelectObject(hbmp_Photo4);
		}
		if((rface>25)&&(rface<31)){
			MemDC.SelectObject(hbmp_Photo5);
		}
		if((rface>30)&&(rface<36)){
			MemDC.SelectObject(hbmp_Photo6);
		}
		if((rface>35)&&(rface<41)){
			MemDC.SelectObject(hbmp_Photo7);
		}
		if((rface>40)&&(rface<46)){
			MemDC.SelectObject(hbmp_Photo8);
		}
		if((rface>45)&&(rface<51)){
			MemDC.SelectObject(hbmp_Photo9);
		}
		DialogDC.BitBlt(488,128,128,128,&MemDC,0,0,SRCAND);//蒙版位图
		DialogDC.BitBlt(488,128,128,128,&MemDC,k,0,SRCINVERT);//图形位图
		pdc->BitBlt(579,351,128,128,&DialogDC,488,128,SRCCOPY);//送屏幕显示
	}
	//////////////////////////////////////////////////////////////////
	//下面一段用于解析prop参数并进行相应的显示
	if(prop!=0){//显示物品，0表示不显示
		switch(prop){
		case 1:MemDC.SelectObject(hbmp_Prop0);break;
		case 2:MemDC.SelectObject(hbmp_Prop1);break;
		case 3:MemDC.SelectObject(hbmp_Prop2);break;
		case 4:MemDC.SelectObject(hbmp_Prop3);break;
		case 5:MemDC.SelectObject(hbmp_Prop4);break;
		case 6:MemDC.SelectObject(hbmp_Prop5);break;
		case 7:MemDC.SelectObject(hbmp_Prop6);break;
		case 8:MemDC.SelectObject(hbmp_Prop7);break;
		case 9:MemDC.SelectObject(hbmp_Prop8);break;
		case 10:MemDC.SelectObject(hbmp_Prop9);break;
		}
		DialogDC.BitBlt(128,0,360,128,&MemDC,0,0,SRCCOPY);//物品直接叠加
		pdc->BitBlt(219,223,360,128,&DialogDC,128,0,SRCCOPY);//送屏幕显示
	}
	//////////////////////////////////////////////////////////////////
	//下面一段用于绘制文字窗口的背景
	BLENDFUNCTION bbf;
	bbf.AlphaFormat = 0;
	bbf.BlendFlags = 0;
	bbf.BlendOp = AC_SRC_OVER;
	bbf.SourceConstantAlpha = 200;
	AlphaBlend( DialogDC.GetSafeHdc(),128,128,360,10,InfoDC.GetSafeHdc(),798,87,1,19,bbf );
	AlphaBlend( DialogDC.GetSafeHdc(),128,246,360,10,InfoDC.GetSafeHdc(),798,87,1,19,bbf );
	AlphaBlend( DialogDC.GetSafeHdc(),128,138,10,108,InfoDC.GetSafeHdc(),798,87,1,19,bbf );
	AlphaBlend( DialogDC.GetSafeHdc(),478,138,10,108,InfoDC.GetSafeHdc(),798,87,1,19,bbf );
	bbf.SourceConstantAlpha = 150;
	AlphaBlend( DialogDC.GetSafeHdc(),138,138,340,108,InfoDC.GetSafeHdc(),799,87,1,19,bbf );
	//////////////////////////////////////////////////////////////////
	//下面绘制文字
	RECT rect;
	DialogDC.SetBkMode(TRANSPARENT);
	rect.left=180;//设定文字的显示范围
	rect.top=155;
	rect.right=487;
	rect.bottom=256;
	//////////////////////////////////////////////////////////////////
	//下面一段用于非系统模式的文字绘制
	if(text!=0){//非系统模式，则显示正常的文本
		char content[128];
		for(i=0;i<128;i++){
			content[i]=dialogtext[(text-1)][i];
		}//从句群中取出需要显示的文本
		//如果是红色、黄色、蓝色的文字，则采用阴影字方案。
		if( mode!=4 && mode!=8 )
		{
			//首先，以黑色绘制文字的阴影
			DialogDC.SetTextColor(0x00000000);
			DialogDC.DrawText(content,32,&rect,DT_LEFT);
			rect.top+=20;
			DialogDC.DrawText(content+32,32,&rect,DT_LEFT);
			rect.top+=20;
			DialogDC.DrawText(content+64,32,&rect,DT_LEFT);
			rect.top+=20;
			DialogDC.DrawText(content+96,32,&rect,DT_LEFT);
			//下面一段用于解析mode参数，给文字设定不同的颜色
			switch(mode){
				case 1:DialogDC.SetTextColor(0x003f3fff);break;//红
				case 5:DialogDC.SetTextColor(0x003f3fff);break;//红
				case 2:DialogDC.SetTextColor(0x0000ffff);break;//黄
				case 6:DialogDC.SetTextColor(0x0000ffff);break;//黄
				case 3:DialogDC.SetTextColor(0x00ffc0c0);break;//蓝
				case 7:DialogDC.SetTextColor(0x00ffc0c0);break;//蓝
				//case 4:DialogDC.SetTextColor(0x0000ff00);break;//绿
				//case 8:DialogDC.SetTextColor(0x0000ff00);break;//绿
				case 9:DialogDC.SetTextColor(0x0000ffff);break;//黄
				case 10:DialogDC.SetTextColor(0x0000ffff);break;//黄
			}
			//然后再以制定颜色绘制文字。
			rect.left=179;
			rect.top=154;
			DialogDC.DrawText(content,32,&rect,DT_LEFT);
			rect.top+=20;
			DialogDC.DrawText(content+32,32,&rect,DT_LEFT);
			rect.top+=20;
			DialogDC.DrawText(content+64,32,&rect,DT_LEFT);
			rect.top+=20;
			DialogDC.DrawText(content+96,32,&rect,DT_LEFT);
		}
		//如果是绿色文字，则采用新研制的“彩色文字”
		else
		{
			//首先绘制黑色的外框
			//注：生成一个文字区域、外框都是黑色的图片
			DialogDC.SetTextColor(0x00000000);
			rect.left=180;
			rect.top=154;
			rect.right=487;
			rect.bottom=256;
			DialogDC.DrawText(content,32,&rect,DT_LEFT);		rect.top+=20;
			DialogDC.DrawText(content+32,32,&rect,DT_LEFT);		rect.top+=20;
			DialogDC.DrawText(content+64,32,&rect,DT_LEFT);		rect.top+=20;
			DialogDC.DrawText(content+96,32,&rect,DT_LEFT);
			rect.left=178;
			rect.top=154;
			rect.right=487;
			rect.bottom=256;
			DialogDC.DrawText(content,32,&rect,DT_LEFT);		rect.top+=20;
			DialogDC.DrawText(content+32,32,&rect,DT_LEFT);		rect.top+=20;
			DialogDC.DrawText(content+64,32,&rect,DT_LEFT);		rect.top+=20;
			DialogDC.DrawText(content+96,32,&rect,DT_LEFT);
			rect.left=179;
			rect.top=153;
			rect.right=487;
			rect.bottom=256;
			DialogDC.DrawText(content,32,&rect,DT_LEFT);		rect.top+=20;
			DialogDC.DrawText(content+32,32,&rect,DT_LEFT);		rect.top+=20;
			DialogDC.DrawText(content+64,32,&rect,DT_LEFT);		rect.top+=20;
			DialogDC.DrawText(content+96,32,&rect,DT_LEFT);
			rect.left=179;
			rect.top=154;
			rect.right=487;
			rect.bottom=256;
			DialogDC.DrawText(content,32,&rect,DT_LEFT);		rect.top+=20;
			DialogDC.DrawText(content+32,32,&rect,DT_LEFT);		rect.top+=20;
			DialogDC.DrawText(content+64,32,&rect,DT_LEFT);		rect.top+=20;
			DialogDC.DrawText(content+96,32,&rect,DT_LEFT);
			rect.left=179;
			rect.top=155;
			rect.right=487;
			rect.bottom=256;
			DialogDC.DrawText(content,32,&rect,DT_LEFT);		rect.top+=20;
			DialogDC.DrawText(content+32,32,&rect,DT_LEFT);		rect.top+=20;
			DialogDC.DrawText(content+64,32,&rect,DT_LEFT);		rect.top+=20;
			DialogDC.DrawText(content+96,32,&rect,DT_LEFT);
			//
			//生成一个文字区域是彩色线条，外面全部是黑色的图片
			CDC MaskDC;
			MaskDC.CreateCompatibleDC( &DialogDC );
			CBitmap bmpMask;
			bmpMask.CreateCompatibleBitmap( &DialogDC, 360, 128 );
			DeleteObject( MaskDC.SelectObject( bmpMask ) );
			DeleteObject( MaskDC.SelectObject(Font) );
			CDC ImageDC;
			ImageDC.CreateCompatibleDC( &DialogDC );
			CBitmap bmpImage;
			bmpImage.LoadBitmap( IDB_BMP_COLORTEXT );
			DeleteObject( ImageDC.SelectObject( bmpImage ) );
			//步骤：
			//1、将MaskDC绘制成底色为黑色，文字为白色的黑白图片
			MaskDC.BitBlt( 0,0,360,128,&MaskDC,0,0,BLACKNESS );
			//2、绘制黑的文字
			MaskDC.SetBkColor(0x00000000);//黑
			MaskDC.SetTextColor(0x00ffffff);//白
			rect.left=51;
			rect.top=26;
			rect.right=487;
			rect.bottom=256;
			MaskDC.DrawText(content,32,&rect,DT_LEFT);			rect.top+=20;
			MaskDC.DrawText(content+32,32,&rect,DT_LEFT);		rect.top+=20;
			MaskDC.DrawText(content+64,32,&rect,DT_LEFT);		rect.top+=20;
			MaskDC.DrawText(content+96,32,&rect,DT_LEFT);
			//3、用“AND”的方式，在Mask上绘制Image
			MaskDC.BitBlt( 0,0,360,128,&ImageDC,0,0,SRCAND );
			//两层或运算，叠加
			DialogDC.BitBlt( 128,128,360,128,&MaskDC,0,0,SRCPAINT );
		}
		//////////////////////////////////////////////////////////////////
	}
	//////////////////////////////////////////////////////////////////
	//下面一段用于系统模式的文字绘制
	if((mode==9)&&(text==0)){//系统模式未指定内容，仅显示一个next按钮
		m_info_st = 6;//游戏初始化
		m_info_next = 1;
		pdc->BitBlt(640,500,130,19,&InfoDC,120,87,SRCCOPY);
	}
	else {
		//下面一段用于展开文字窗口
		disdialogst=1;//允许OnDraw函数绘制对话框
		pdc->BitBlt(219,351,360,128,&DialogDC,128,128,SRCCOPY);
	}
	
	//下面一段用于设置响应模式
	if(mode<5){//设定为next模式
		m_info_next = 1;
		//m_info_map=0;//不允许点击地图
	}

	//存盘操作的专用对话
	if(mode == 5) {
		m_info_next = 1;
		SaveGame();
	}

	RenewInfo(pdc);//使按钮使能
	Sleep(500);
	ReleaseDC(pdc);
	pdc = NULL;
}
//-█/////////////////////////////////////////////////////////////////////////////
//███████████████████████████████████████
//计算攻击力度
//-█/////////////////////////////////////////////////////////////////////////////
void CMainFrame::GetPower(int type,int *power)
{
	if(type==1){//用枪，必杀
		*power=(10000);
	}
	else if(type==2){//拳头
		*power=(level*2);//等级
	}
	else if(type==3){//短刀
		*power=(level*5);
	}
}
//-█/////////////////////////////////////////////////////////////////////////////
//███████████████████████████████████████
//绘制大地图
//+█/////////////////////////////////////////////////////////////////////////////
void CMainFrame::DrawMap(void)
{
	int sa=(current.x-13);
	int a=sa;
	int b=(current.y-8);//左上的单元编号
	int sc;
	int d;//MapDC左上点坐标
	int i;
	switch(movest){
	case 0:sc=-32;d=-32;break;
	case 1:sc=-32;d=-24;break;
	case 2:sc=-32;d=-16;break;
	case 3:sc=-32;d=-8;break;
	case 4:sc=-32;d=0;break;
	case 5:sc=-32;d=-40;break;
	case 6:sc=-32;d=-48;break;
	case 7:sc=-32;d=-56;break;
	case 8:sc=-32;d=-64;break;
	case 9:sc=-24;d=-32;break;
	case 10:sc=-16;d=-32;break;
	case 11:sc=-8;d=-32;break;
	case 12:sc=0;d=-32;break;
	case 13:sc=-40;d=-32;break;
	case 14:sc=-48;d=-32;break;
	case 15:sc=-56;d=-32;break;
	case 16:sc=-64;d=-32;break;
	default:sc=-32;d=-32;break;
	}
	int c=sc;
	for(int m=0;m<17;m++){
		for(int n=0;n<27;n++){
			if((a<200)&&(a>=0)&&(b<200)&&(b>=0)){
				i=map[a][b];
				if((i<801)&&(i>0)){//i的范围从0到800
					i--;
					int sx=((i%40)*32);
					int sy=((i/40)*32);
					MapDC.BitBlt(c,d,32,32,&CilDC,sx,sy,SRCCOPY);//绘制
				}
				else{
					MapDC.BitBlt(c,d,32,32,&CilDC,0,0,BLACKNESS);//未知数值区域使用黑色
				}

#ifdef DEBUG//在有事件的地方画一个小黑格子
				int t=npc[a][b];
				if((t<801)&&(t>=0)){//t的范围从0到800
					t--;
					int sx=((t%40)*32);
					int sy=((t/40)*32);
					MapDC.BitBlt(c,d,32,32,&EvtDC,sx,sy,SRCPAINT);//绘制事件格子
				}
#endif //DEBUG
			}
			a+=1;
			c+=32;
		}
		a=sa;
		b+=1;
		c=sc;
		d+=32;
	}
}
//-█/////////////////////////////////////////////////////////////////////////////
//███████████████████████████████████████
//███████████████████████████████████████
//存储游戏进度
//+█/////////////////////////////////////////////////////////////////////////////
void CMainFrame::SaveGame()
{
	FILE *fp;
	if((fp=fopen("game.sav","wb"))==NULL)
	{
		MessageBox("创建存盘文件失败，请检查文件夹属性！");
		Dialog(9,0,0,0,0);
	}
	else
	{
		DWORD temp[60];
		//
		temp[0]=blood;
		temp[1]=level;
		temp[2]=bullet;
		temp[3]=st;
		temp[4]=current.x;
		temp[5]=current.y;
		temp[6]=azimuth;
		temp[7]=st_sub1;
		temp[8]=st_sub2;
		temp[9]=m_info_prop1;
		temp[10]=m_info_prop2;
		temp[11]=m_info_prop3;
		temp[12]=m_info_prop4;
		temp[13]=st_dialog;
		temp[14]=st_progress;
		temp[15]=m_oldinfo_st;
		temp[16]=m_oldinfo_map;
		temp[17]=m_oldinfo_next;
		temp[18]=0;
		temp[19]=m_info_st;
		temp[20]=m_info_map;
		temp[21]=m_info_next;
		temp[22]=0;
		temp[23]=base_x;
		temp[24]=base_y;
		temp[25]=score;
		//
		fwrite(temp,sizeof(DWORD),60,fp);
		fclose(fp);
	}
}
//-█/////////////////////////////////////////////////////////////////////////////
//███████████████████████████████████████
//读取游戏进度
//+█/////////////////////////////////////////////////////////////////////////////
BOOL CMainFrame::LoadGame()
{
	FILE *fp;
	if((fp=fopen("game.sav","rb"))==NULL)
	{
		return FALSE;
	}
	else
	{
		DWORD temp[60];
		fread(temp,sizeof(DWORD),60,fp);
		fclose(fp);
		//
		blood=temp[0];
		level=temp[1];
		bullet=temp[2];
		st=temp[3];
		current.x=temp[4];
		current.y=temp[5];
		azimuth=temp[6];
		st_sub1=temp[7];
		st_sub2=temp[8];
		m_info_prop1=temp[9];
		m_info_prop2=temp[10];
		m_info_prop3=temp[11];
		m_info_prop4=temp[12];
		st_dialog=temp[13];
		st_progress=temp[14];
		m_oldinfo_st=temp[15];
		m_oldinfo_map=temp[16];
		m_oldinfo_next=temp[17];
		//temp[18];
		m_info_st=temp[19];
		m_info_map=temp[20];
		m_info_next=temp[21];
		//temp[22];
		base_x=temp[23];
		base_y=temp[24];
		score=temp[25];
	}

	return TRUE;
}
//-█/////////////////////////////////////////////////////////////////////////////
//播放MIDI音乐
//+█/////////////////////////////////////////////////////////////////////////////
void CMainFrame::PlayMidi(char *fname)//	void CMainFrame::PlaySound(char *fname);
{
	//用于绘制“请稍候……”
	if((m_info_st==8)||(m_info_st==10)||(m_info_st==100))
	{
		RECT rect;
		rect.left=0;
		rect.top=400;
		rect.right=799;
		rect.bottom=479;
		CDC *pdc=GetDC();
		pdc->SetTextColor(0x0000ffff);//黄色
		pdc->SetBkMode(TRANSPARENT);//设置透明背景
		HANDLE hOldObj = pdc->SelectObject(Font);
		pdc->DrawText("请稍侯……",10,&rect,DT_CENTER);
		pdc->SelectObject(hOldObj);
		ReleaseDC(pdc);
		pdc = NULL;
	}

	//以下是播放MIDI所需要的语句
	if (m_pMidi) {
		m_pMidi->PlayMidi(fname);
	}
}
//-█/////////////////////////////////////////////////////////////////////////////
//播放WAV声音
//+█/////////////////////////////////////////////////////////////////////////////
void CMainFrame::PlayWave(char *fname)
{
	if (m_pMidi) {
		m_pMidi->PlayWave(fname);
	}
}
//-█/////////////////////////////////////////////////////////////////////////////
LRESULT CMainFrame::WindowProc(UINT message, WPARAM wParam, LPARAM lParam)
{
	// TODO: 在此添加专用代码和/或调用基类
	switch (message) 
	{
	case MM_MCINOTIFY:
		m_pMidi->ReplayMidi(wParam);
		break;
	case WM_DESTROY:
		m_pMidi->StopMidi();	//退出时关闭Midi
		break;
	default:
		return CFrameWnd::WindowProc(message, wParam, lParam);
   }
   return 1;
}
//███████████████████████████████████████
//███████████████████████████████████████
//█五种关于情节解析的处理函数定义如下█████████████████████
//███████████████████████████████████████
void CMainFrame::InitGame(void)
{
	if (LoadGame()) {//读取存盘文件成功
		CDC *pdc=GetDC();
		BuffDC.BitBlt(0,0,800,480,&MapDC,0,0,BLACKNESS);
		pdc->BitBlt(0,0,800,480,&BuffDC,0,0,SRCCOPY);
		//用于绘制“请稍候……”
		RECT rect;
		rect.left=0;
		rect.top=400;
		rect.right=799;
		rect.bottom=479;
		pdc->SetTextColor(0x0000ffff);//黄色
		pdc->SetBkMode(TRANSPARENT);//设置透明背景
		HANDLE hOldObj = pdc->SelectObject(Font);
		pdc->DrawText("请稍侯……",10,&rect,DT_CENTER);
		pdc->SelectObject(hOldObj);
		//
		ReleaseDC(pdc);
		pdc = NULL;
	}
	else {//开始新的游戏
		blood=0;//生命
		level=0;//等级
		bullet=0;//子弹数量
		st=2;//系统状态号，2表示第一关
		azimuth= AZIMUTH_DOWN;//主角的方位。1上2下3左4右
		st_sub1=0;
		st_sub2=0;
		m_info_prop1=0;
		m_info_prop2=0;
		m_info_prop3=0;
		m_info_prop4=0;
		st_dialog=0;//对话进度号
		st_progress=0;//细节进度号
	}
	
	StopMidiPlay();
	if(st==2){Para1Init();}
	else if(st==3){Para2Init();}
	else if(st==4){Para3Init();}
	else if(st==5){Para4Init();}
	else if(st==6){Para5Init();}
	else if(st==7){Para6Init();}
	else if(st==8){Para7Init();}
	else if(st==9){Para8Init();}
	OnInfo(1);
}
//███████████████████████████████████████
void CMainFrame::OnShoot(int power,CPoint point)
{
	int i;
	for (i=0 ; i<50 ; i++ ){
		m_enemies[i]->Dowound(npc,power,point,current,&score);
	}
}
//███████████████████████████████████████
void CMainFrame::OnInfo(int type)
{
	if(st==2){Para1Info(type);}
	else if(st==3){Para2Info(type);}
	else if(st==4){Para3Info(type);}
	else if(st==5){Para4Info(type);}
	else if(st==6){Para5Info(type);}
	else if(st==7){Para6Info(type);}
	else if(st==8){Para7Info(type);}
	else if(st==9){Para8Info(type);}
}
//███████████████████████████████████████
LRESULT CMainFrame::OnAccident(WPARAM type, LPARAM reserve)
{
	//战斗中有时会发出编号为800或810的事件(Hero移动占位格)，滤掉不处理
	if(800==type||810==type) return 0;

	if(messagedelay==0){//如果连续触发消息，消息将不被执行
	if(st==2){Para1Accident(type);}
	else if(st==3){Para2Accident(type);}
	else if(st==4){Para3Accident(type);}
	else if(st==5){Para4Accident(type);}
	else if(st==6){Para5Accident(type);}
	else if(st==7){Para6Accident(type);}
	else if(st==8){Para7Accident(type);}
	else if(st==9){Para8Accident(type);}
	}
	messagedelay=10;
	return 0;
}
//███████████████████████████████████████
//void CMainFrame::OnTimer(UINT nIDEvent)
//{
//	if(nIDEvent==GAME_TIMER_ID){
//		ActProc();
//	}
//}

void CMainFrame::ActProc(void)
{
	CDC *pdc=GetDC();

	//鼠标和键盘同时操作时，只处理一次，而且键盘优先
	BOOL bWalkDown = FALSE;
	BOOL bFightDown = FALSE;

	//以下一段用于检测按键
	if(GetAsyncKeyState(VK_UP) || GetAsyncKeyState('W')){
		GoUp();		bWalkDown = TRUE;
	}
	else if(GetAsyncKeyState(VK_DOWN) || GetAsyncKeyState('S')){
		GoDown();	bWalkDown = TRUE;
	}
	else if(GetAsyncKeyState(VK_LEFT) || GetAsyncKeyState('A')){
		GoLeft();	bWalkDown = TRUE;
	}
	else if(GetAsyncKeyState(VK_RIGHT) || GetAsyncKeyState('D')){
		GoRight();	bWalkDown = TRUE;
	}
	else if(GetAsyncKeyState(VK_SPACE)&&(m_info_st==10)){
		Fight();	bFightDown = TRUE;
	}

	//根据鼠标状态做出处理。键盘处理比鼠标优先，如果键盘动作已经发生，则不重复处理
	if ((! bFightDown)&&(! bWalkDown)&&(m_info_st==10)) {//键盘没有战斗操作，执行鼠标操作

		if (GetAsyncKeyState(VK_RBUTTON)<0)//鼠标右键按着，执行战斗动作（拳或刀）
		{
			if ((m_mouse_pointer.x>0)&&
				(m_mouse_pointer.x<800)&&
				(m_mouse_pointer.y>0)&&
				(m_mouse_pointer.y<480)){

				azimuth=m_mouse_azimuth;
				Fight();
				bFightDown=TRUE;
			}
		}
	}
	
	if ((! bFightDown)&&(! bWalkDown)&&
		((m_info_st==100)||(m_info_st==10)||(m_info_st==8)))//状态上允许行走
	{//鼠标左键按着，执行行走动作。但是战斗比行走优先，如果执行了战斗动作就不走了

		if ((GetAsyncKeyState(VK_LBUTTON)<0)) {//鼠标左键按着，执行行走动作

			if ((m_mouse_pointer.x>0)&&
				(m_mouse_pointer.x<800)&&
				(m_mouse_pointer.y>0)&&
				(m_mouse_pointer.y<480)){

				//azimuth  1上2下3左4右;5上走6下走7左走8右走
				//如果撞墙了，要根据鼠标的位置，选择从旁边绕过
				switch (m_mouse_azimuth) {
				case AZIMUTH_UP:
					if (! GoUp()) {
						if (m_mouse_pointer.x < 400) { GoLeft(); } else { GoRight(); }
					}
					break;
				case AZIMUTH_DOWN:
					if (! GoDown()) {
						if (m_mouse_pointer.x < 400) { GoLeft(); } else { GoRight(); }
					}
					break;
				case AZIMUTH_LEFT:
					if (! GoLeft()) {
						if (m_mouse_pointer.y < 208) { GoUp(); } else { GoDown(); }
					}
					break;
				case AZIMUTH_RIGHT:
					if (! GoRight()) {
						if (m_mouse_pointer.y < 208) { GoUp(); } else { GoDown(); }
					}
					break;
				}
			}
		}
	}

	//信息栏显示内容刷新
	InfoDC.BitBlt(550,110,200,9,&InfoDC,190,39,SRCCOPY);//背景
	InfoDC.StretchBlt(550,110,blood,9,&InfoDC,746,87,1,9,SRCCOPY);//生命
	pdc->BitBlt(190,500,200,9,&InfoDC,550,110,SRCCOPY);//显示
	InfoDC.BitBlt(550,110,200,9,&InfoDC,190,39,SRCCOPY);//背景
	InfoDC.StretchBlt(550,110,level,9,&InfoDC,773,87,1,9,SRCCOPY);//等级
	InfoDC.StretchBlt(550,110,score,9,&InfoDC,777,87,1,9,SRCCOPY);//分数
	pdc->BitBlt(190,519,200,9,&InfoDC,550,110,SRCCOPY);//显示
	if(azimuth>4){
		MoveActor();//同时显示各种敌人
	}
	else{
		BuffDC.BitBlt(0,0,800,480,&MapDC,0,0,SRCCOPY);//将地图复制到缓冲页面	
		if(m_info_st==8){
			DrawActor(1);
		}
		else if(m_info_st==10){
			displayenemies();//显示各种敌人
			DrawActor(1);
		}
		else if(m_info_st==100){
			displayenemies();//显示各种敌人
			displayFightingHero(ShootingStatus,0,&BuffDC);//显示不同方向射击图片
		}
	}

  	pdc->BitBlt(0,0,800,480,&BuffDC,0,0,SRCCOPY);//绘好的地图缓冲区送屏幕显示
	if((blood<=0)&&((m_info_st==10)||(m_info_st==100))){//牺牲了
		gameover();
	}
	if((score>=level)&&((m_info_st==10)||(m_info_st==100))){//升级
		levelup();
	}
	if(messagedelay>0){messagedelay--;}//处理一次NPC多次响应的问题

	ReleaseDC(pdc);
	pdc = NULL;
}
void CMainFrame::SetActTimer(void)
{
	//SetTimer(GAME_TIMER_ID,40,NULL);

	theApp.m_EnableActProc = TRUE;
}
void CMainFrame::StopActTimer(void)
{
	//KillTimer(GAME_TIMER_ID);

	theApp.m_EnableActProc = FALSE;
}
void CMainFrame::GameSleep(int ms)
{
	theApp.WasteMessage();//避免操作系统认为本程序失去响应
	Sleep(ms);
}
//███████████████████████████████████████
//显示各种敌人
void CMainFrame::displayenemies(void)
{
	if(st==2){Para1Timer();}
	else if(st==3){Para2Timer();}
	else if(st==4){Para3Timer();}
	else if(st==5){Para4Timer();}
	else if(st==6){Para5Timer();}
	else if(st==7){Para6Timer();}
	else if(st==8){Para7Timer();}
	else if(st==9){Para8Timer();}
}
//███████████████████████████████████████
void CMainFrame::OnExit() 
{
	if(m_info_st!=100){
		CPoint point;
		point.x=35;
		point.y=533;
		OnLButtonDown(NULL,point);
	}
}
//
void CMainFrame::OnMap() 
{
	if(m_info_st!=100){
		CPoint point;
		point.x=35;
		point.y=505;
		OnLButtonDown(NULL,point);
	}
}
//
void CMainFrame::OnSnapshot() 
{
	if(m_info_st!=100){
		CPoint point;
		point.x=715;
		point.y=533;
		OnLButtonDown(NULL,point);
	}
}
//
void CMainFrame::OnKill() 
{
	if(m_info_st!=100){//等于Next按钮的作用
		CPoint point;
		point.x=645;
		point.y=505;
		OnLButtonDown(NULL,point);
	}
}
//
//███████████████████████████████████████
BOOL CMainFrame::CreateSnapshotFolder(CString sDir)
{
	BOOL bRet = TRUE;

	WIN32_FIND_DATA fd;
	HANDLE hFind = FindFirstFile(sDir, &fd);
    if (INVALID_HANDLE_VALUE == hFind) {

		// 目录不存在，创建目录
		SECURITY_ATTRIBUTES attrib;
		attrib.bInheritHandle = FALSE;
		attrib.lpSecurityDescriptor = NULL;
		attrib.nLength = sizeof(SECURITY_ATTRIBUTES);
 
		bRet = ::CreateDirectory (sDir, &attrib);
	}
    FindClose(hFind);
	return bRet;
}

//███████████████████████████████████████
void CMainFrame::OnLButtonDblClk(UINT nFlags, CPoint point)
{
	if (2==m_info_map) {
		//显示地图的时候，什么都不处理
		return;
	}

	// 右键双击，换武器
	if((point.y<480)&&(m_info_prop4==5)){//如果处在游戏区而且有枪的话，就可以进入射击状态
		if(m_info_st==10){
			m_info_st=100;//进入射击态
			::SetCursor(cur1);//采用准星光标
		}
		else if(m_info_st==100){
			m_info_st=10;//返回战斗态
			::SetCursor(cur0);//采用箭头光标
		}
	}
	CFrameWnd::OnLButtonDblClk(nFlags, point);
}
//███████████████████████████████████████
BOOL CMainFrame::OnSetCursor(CWnd* pWnd, UINT nHitTest, UINT message)
{
	return 0;//告诉MFC已经设置好了光标，不要再设置一次了
}

void CMainFrame::OnNcMouseMove(UINT nHitTest, CPoint point)
{
	//鼠标在标题栏的时候，取消瞄准镜恢复箭头光标
	::SetCursor(cur0);
	CFrameWnd::OnNcMouseMove(nHitTest, point);
}
//███████████████████████████████████████