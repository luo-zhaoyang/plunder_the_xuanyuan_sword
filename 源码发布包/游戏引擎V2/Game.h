// Game.h : Game 应用程序的主头文件
//
#pragma once

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif


#define ID_GAME_MESSAGE       WM_USER+100



#include "resource.h"       // 主符号

#include "macro.h"
#include "Midi.h"
#include "GameObject.h"
#include "Hero.h"
#include "Npc.h"
#include "Boss.h"
#include "GameMap.h"
#include "MainFrm.h"
#include "Script.h"
#include "Bmp.h"
#include "Avi.h"

// CGameApp:
// 有关此类的实现，请参阅 Game.cpp
//

class CGameApp : public CWinApp
{
private:
	long m_lLastRenewScreenTime;    // 上次重绘屏幕的时间
	long m_lInactiveTime;           // 系统进入非活动状态的开始时间
	long m_lInactiveCount;          // 系统的累计非活动时间

public:
	CGameApp();

// 重写
public:
	virtual BOOL InitInstance();

// 实现

public:
	afx_msg void OnAppAbout();
	DECLARE_MESSAGE_MAP()
	virtual BOOL OnIdle(LONG lCount);
};

extern CGameApp theApp;
// 绘制透明图片
extern void TransBlt( HDC hdc,int ox,int oy,int cx,int cy,HDC hMemDC,int sx,int sy,COLORREF colorkey );
// 判断一个点是否落在一个矩形区域中
extern BOOL PtInArea( int ptx, int pty, int x, int y, int w, int h );
// 检验两个矩形区域是否相交
extern BOOL IsAreaCut( int x1, int y1, int w1, int h1, int x2, int y2, int w2, int h2 );
// 从字符串中取得一行
extern BOOL GetLine( CString& strReturn, CString& strSource, int k );
// 从字符串中取得两个“;”之间的部分
extern BOOL GetSlice( CString& strReturn, CString& strSource, int k );

/* END */
