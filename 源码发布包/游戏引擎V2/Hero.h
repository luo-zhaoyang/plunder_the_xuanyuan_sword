// Hero.h: interface for the CHero class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(__HERO_H__)
#define __HERO_H__


// 动作状态与坐标转换表
const int hero_act_x[12]	= {32,64, 0,32,64,0,32,64, 0, 32, 64,  0};
const int hero_act_y[12] = {96,96,96, 0, 0,0,48,48,48,144,144,144};

class CHero : public CGameObject
{
public:
	// 定义当前需要执行的动作
	WORD m_wAct;
	// 0x00:无动作
	// 0x01:向上走
	// 0x02:向下走
	// 0x03:向左走
	// 0x04:向右走
	// 0x10:开火
	// 0x20:更换弹夹
	// 0x80:动作执行完成标志
	// 注意：5、6两个动作与行走动作可以并行发生

	// 当前动作状态的代号
	int m_nStatus;
	//  1:无枪，面上迈步1； 21:持枪，面上迈步1； 41:射击，面上迈步1；
	//  2:无枪，面上迈步2； 22:持枪，面上迈步2； 42:射击，面上迈步2；
	//  3:无枪，面上站立；  23:持枪，面上站立；  43:射击，面上站立；
	//  4:无枪，面下迈步1； 24:持枪，面下迈步1； 44:射击，面下迈步1；
	//  5:无枪，面下迈步2； 25:持枪，面下迈步2； 45:射击，面下迈步2；
	//  6:无枪，面下站立；  26:持枪，面下站立；  46:射击，面下站立；
	//  7:无枪，面左迈步1； 27:持枪，面左迈步1； 47:射击，面左迈步1；
	//  8:无枪，面左迈步2； 28:持枪，面左迈步2； 48:射击，面左迈步2；
	//  9:无枪，面左站立；  29:持枪，面左站立；  49:射击，面左站立；
	// 10:无枪，面右迈步1； 30:持枪，面右迈步1； 50:射击，面右迈步1；
	// 11:无枪，面右迈步2； 31:持枪，面右迈步2； 51:射击，面右迈步2；
	// 12:无枪，面右站立；  32:持枪，面右站立；  52:射击，面右站立；
	// 无枪动作图片的y值加192为持枪动作图片，加384为射击动作图片。
	//  100:死亡动画开始；108:死亡动画结束，共9桢。

	// 生命
	int m_nLife;
	// 等级
	int m_nLevel;
	// 经验
	int m_nExp;

	// 副角色图片设备
	HDC m_hImageDC2;

public:
	CHero( CGameMap* pGameMap );
	virtual ~CHero();

	// 初始化副角色图片设备
	BOOL InitImage2( char* psFileName );

	// 判断副角色是否存在
	BOOL HaveHero2();

	// 绘制
	void Draw( HDC hDC );

	// 获得所在格子的坐标。每个精灵只能占一个格子的位置
	// 得到以格子为单位的坐标系统（用于地图绘制和碰撞检测）
	BOOL GetGride( int* pX, int* pY );

	// 得到碰撞检测矩形
	BOOL GetHitTestRect( int* x, int* y, int* w, int* h );

	// 持枪/无枪的设置
	BOOL SetGun( BOOL bHoldGun );

	// 测试角色手中是否有枪
	BOOL HaveGun();

	// 设置动作
	BOOL SetAct( WORD wAct, long lNow );

	// 移动
	void Move( long lNow );

	// 移动到新位置
	void MoveToNewPos( WORD wAct );
};

#endif // !defined(__HERO_H__)
